INSERT INTO game (game_name, game_description, game_state, game_nw_lat, game_nw_lng, game_se_lat, game_se_lng) VALUES ('Game 1', '', 0, 59.919335551796145, 17.5352749280334, 59.79841050520314, 17.78095396012414);
INSERT INTO game (game_name, game_description, game_state, game_nw_lat, game_nw_lng, game_se_lat, game_se_lng) VALUES ('Game 2', '', 2, 59.34719322341846, 15.03793289953789, 59.16571492588011, 15.483147928090228);
INSERT INTO game (game_name, game_description, game_state, game_nw_lat, game_nw_lng, game_se_lat, game_se_lng) VALUES ('Game 3', '', 1, 58.70387372228334, 16.025981149288167, 58.52169480770446, 16.306845124636915);
INSERT INTO game (game_name, game_description, game_state, game_nw_lat, game_nw_lng, game_se_lat, game_se_lng) VALUES ('Game 4', '', 1, 57.76424810228386, 12.837030860902388, 57.68432760765887, 13.024761702219948);
INSERT INTO game (game_name, game_description, game_state, game_nw_lat, game_nw_lng, game_se_lat, game_se_lng) VALUES ('Ålands Apokalypsen', 'The official Åland Apocalypse!', 0, 60.41785340187117, 19.52777179534652, 59.968045991220016, 20.5676449348106);
INSERT INTO game (game_name, game_description, game_state, game_nw_lat, game_nw_lng, game_se_lat, game_se_lng) VALUES ('Hönö Spelet', 'Humans vs Zombies game played on Hönö, Öckerö and other isles', 1, 57.79077303971261, 11.597677064542427, 57.63883614229489, 11.695554115921967);
INSERT INTO game (game_name, game_description, game_state, game_nw_lat, game_nw_lng, game_se_lat, game_se_lng) VALUES ('Landvetter Flygplats', 'The zombie outbreak has started at Landvetter Airport!', 1, 57.64819401071629, 12.263091111694157, 57.67853811092899, 12.309427279060426);
INSERT INTO game (game_name, game_description, game_state, game_nw_lat, game_nw_lng, game_se_lat, game_se_lng) VALUES ('Adrians hemtrakter', '', 1, 60.13090203372381, 19.88305330918305, 60.04542880510542, 20.00761451441466);

INSERT INTO mission (mission_name, mission_lat, mission_lng, mission_visible_by_human, mission_visible_by_zombie, game_id) VALUES ('Save the children in Strandnäs skola', 60.11710183770201, 19.943868764433674, true, false, 8);
INSERT INTO mission (mission_name, mission_lat, mission_lng, mission_visible_by_human, mission_visible_by_zombie, game_id) VALUES ('Secure Pizzeria Diablo', 60.10015519648271, 19.942596052888202, true, false, 8);
INSERT INTO mission (mission_name, mission_lat, mission_lng, mission_visible_by_human, mission_visible_by_zombie, game_id) VALUES ('Destroy Pizzeria Diablo', 60.10015519648271, 19.942596052888202, false, true, 8);
INSERT INTO mission (mission_name, mission_lat, mission_lng, mission_visible_by_human, mission_visible_by_zombie, game_id) VALUES ('Gravestone Marker', 60.09780637923288, 19.92537452419208, true, true, 8);
INSERT INTO mission (mission_name, mission_lat, mission_lng, mission_visible_by_human, mission_visible_by_zombie, game_id) VALUES ('Gravestone Marker', 60.10229269300101, 19.935193630229538, true, true, 8);
INSERT INTO mission (mission_name, mission_lat, mission_lng, mission_visible_by_human, mission_visible_by_zombie, game_id) VALUES ('Gravestone Marker', 60.08519662281359, 19.947939487373986, true, true, 8);
INSERT INTO mission (mission_name, mission_lat, mission_lng, mission_visible_by_human, mission_visible_by_zombie, game_id) VALUES ('Gravestone Marker', 60.07684863607113, 19.947767826288125, true, true, 8);
INSERT INTO mission (mission_name, mission_lat, mission_lng, mission_visible_by_human, mission_visible_by_zombie, game_id) VALUES ('Gravestone Marker', 60.071796007449734, 19.958110423857637, true, true, 8);

INSERT INTO player (is_human, is_patient_zero, bite_code, game_id) VALUES (true, false, '12031hoa', 2);
INSERT INTO player (is_human, is_patient_zero, bite_code, game_id) VALUES (false, false, '0134', 1);
INSERT INTO player (is_human, is_patient_zero, bite_code, game_id) VALUES (true, false, '1212341', 4);
INSERT INTO player (is_human, is_patient_zero, bite_code, game_id) VALUES (false, false, 'jiiojaw', 3);
INSERT INTO player (is_human, is_patient_zero, bite_code, game_id) VALUES (false, false, '234019', 5);
INSERT INTO player (is_human, is_patient_zero, bite_code, game_id) VALUES (true, false, 'j12341', 2);
INSERT INTO player (is_human, is_patient_zero, bite_code, game_id) VALUES (true, true, '02319', 2);
INSERT INTO player (is_human, is_patient_zero, bite_code, game_id) VALUES (false, false, '89089080', 2);
INSERT INTO player (is_human, is_patient_zero, bite_code, game_id) VALUES (false, false, '8y998y89y', 2);