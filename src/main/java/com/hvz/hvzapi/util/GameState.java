package com.hvz.hvzapi.util;

public enum GameState {
    REGISTRATION, IN_PROGRESS, COMPLETE
}
