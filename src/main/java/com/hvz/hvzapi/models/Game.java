package com.hvz.hvzapi.models;

import com.hvz.hvzapi.util.GameState;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Game {
    @Id
    @Column(name = "game_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "game_name", nullable = false)
    private String gameName;

    @Column(name = "game_description")
    private String description;

    @Column(name = "game_state", nullable = false)
    private GameState gameState;

    @Column(name = "game_nw_lat", nullable = false)
    private double nwLat;

    @Column(name = "game_nw_lng", nullable = false)
    private double nwLng;

    @Column(name = "game_se_lat", nullable = false)
    private double seLat;

    @Column(name = "game_se_lng", nullable = false)
    private double seLng;

    @OneToMany(mappedBy = "game")
    private Set<Mission> missions;

    @OneToMany(mappedBy = "game")
    private Set<Player> players;
    
    @OneToMany(mappedBy = "game")
    private Set<Kill> kills;
    
    @OneToMany(mappedBy = "game")
    private Set<ChatMessage> messages;
}
