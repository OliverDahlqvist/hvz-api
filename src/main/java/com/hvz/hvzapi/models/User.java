package com.hvz.hvzapi.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "hvz_user")
@Getter
@Setter
@NoArgsConstructor
public class User {
	@Id
	@Column(name = "user_uid")
	private String uid;
	
	@Column(length = 80, nullable = false, name = "first_name")
	private String firstName;
	
	@Column(length = 80, nullable = false, name = "last_name")
	private String lastName;
	
	@OneToMany(mappedBy = "hvzUser")
	private Set<Player> players;
}
