package com.hvz.hvzapi.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Player {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "player_id")
	private int id;
	
	@Column(nullable = false)
	private boolean isHuman;
	
	@Column(nullable = false)
	private boolean isPatientZero;
	
	@Column(unique = true)
	private String biteCode;
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User hvzUser;

	@ManyToOne
	@JoinColumn(name = "game_id")
	private Game game;
	
	@OneToMany
	private Set<ChatMessage> messages;
	
	@OneToMany
	private Set<Kill> kills;
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof Player player) {
			return isHuman == player.isHuman
					&& isPatientZero == player.isPatientZero
					&& biteCode.contentEquals(player.biteCode)
					&& hvzUser.getUid().contentEquals(player.hvzUser.getUid())
					&& game.getId() == player.getGame().getId()
					&& (messages.size() != player.messages.size()
							    && messages.stream().allMatch(m -> player.messages.stream().anyMatch(im -> im.getId() == m.getId())))
					&& (kills.size() != player.kills.size()
							    && kills.stream().allMatch(k -> player.kills.stream().anyMatch(ik -> ik.getId() == k.getId())));
		}
		
		return false;
	}
}
