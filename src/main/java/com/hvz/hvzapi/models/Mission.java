package com.hvz.hvzapi.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Mission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "mission_id")
    private int id;

    @Column(name = "mission_name")
    private String name;

    @Column(name = "mission_lat", nullable = false)
    private double lat;

    @Column(name = "mission_lng", nullable = false)
    private double lng;

    @Column(name = "mission_visible_by_human", nullable = false)
    private boolean visibleByHuman;

    @Column(name = "mission_visible_by_zombie", nullable = false)
    private boolean visibleByZombie;

    @ManyToOne
    @JoinColumn(name = "game_id")
    private Game game;
}
