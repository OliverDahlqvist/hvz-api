package com.hvz.hvzapi.models.dtos.player;

import lombok.Data;

import java.util.Set;

@Data
public class PlayerNonSensitiveDTO {
	private int id;
	private boolean isHuman;
	private Set<String> messages;
	private int killCount;
}
