package com.hvz.hvzapi.models.dtos.kill;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class KillDTO {
	private int id;
	private Timestamp killTime;
	private String story;
	private double lat;
	private double lng;
	private Integer killerId;
	private Integer victimId;
	private Integer gameId;
}
