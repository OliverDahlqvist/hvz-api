package com.hvz.hvzapi.models.dtos.kill;

import lombok.Data;

@Data
public class KillAddByBiteCodeDTO {
	private String story;
	private String biteCode;
	private double lat;
	private double lng;
}
