package com.hvz.hvzapi.models.dtos.player;

import lombok.Data;

import java.util.Set;

@Data
public class PlayerDTO {
	private int id;
	private boolean isHuman;
	private boolean isPatientZero;
	private String biteCode;
	private String userId;
	private Integer game;
	private Set<Integer> messageIds;
	private Set<Integer> killIds;
}
