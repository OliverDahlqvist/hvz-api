package com.hvz.hvzapi.models.dtos.chat;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class ChatMessageDTO {
	private int id;
	private String message;
	private boolean isZombieGlobal;
	private boolean isHumanGlobal;
	private Timestamp messageTime;
	private int senderId;
	private int gameId;
}
