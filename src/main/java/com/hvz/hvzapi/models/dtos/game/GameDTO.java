package com.hvz.hvzapi.models.dtos.game;

import lombok.Data;
import java.util.Set;

@Data
public class GameDTO {
    private int id;
    private String gameName;
    private String description;
    private Integer gameState;
    private double nwLat;
    private double nwLng;
    private double seLat;
    private double seLng;
    private Set<Integer> missions;
    private Set<Integer> players;
    private Set<Integer> kills;
}
