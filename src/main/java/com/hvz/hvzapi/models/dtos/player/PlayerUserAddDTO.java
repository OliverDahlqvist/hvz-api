package com.hvz.hvzapi.models.dtos.player;


import lombok.Data;

@Data
public class PlayerUserAddDTO {
	private boolean isHuman;
}
