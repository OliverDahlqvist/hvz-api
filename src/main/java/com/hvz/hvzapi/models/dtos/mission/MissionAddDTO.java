package com.hvz.hvzapi.models.dtos.mission;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MissionAddDTO {
    private String name;
    private double lat;
    private double lng;
    private boolean visibleByHuman;
    private boolean visibleByZombie;
    private Integer gameId;
    
    public MissionAddDTO(String name, double lat, double lng, boolean visibleByHuman,
                         boolean visibleByZombie, Integer gameId) {
        this.name = name;
        this.lat = lat;
        this.lng = lng;
        this.visibleByHuman = visibleByHuman;
        this.visibleByZombie = visibleByZombie;
        this.gameId = gameId;
    }
}
