package com.hvz.hvzapi.models.dtos.player;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
public class PlayerAddDTO {
	private boolean isHuman;
	private boolean isPatientZero;
	private String userId;
	private Integer gameId;
	private Set<Integer> messageIds;
	private Set<Integer> killIds;
}
