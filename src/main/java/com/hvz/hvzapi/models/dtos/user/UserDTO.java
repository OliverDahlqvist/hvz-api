package com.hvz.hvzapi.models.dtos.user;


import lombok.Data;

import java.util.Set;

@Data
public class UserDTO {
	private String uid;
	private String firstName;
	private String lastName;
	private Set<Integer> playerIds;
}
