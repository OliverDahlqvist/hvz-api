package com.hvz.hvzapi.models.dtos.player;

import lombok.Data;

@Data
public class PlayerEditDTO {
	private int id;
	private boolean isHuman;
}
