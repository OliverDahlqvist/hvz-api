package com.hvz.hvzapi.models.dtos.game;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
public class GameAddDTO {
	private String gameName;
	private String description;
	private Integer gameState;
	private double nwLat;
	private double nwLng;
	private double seLat;
	private double seLng;
	private Set<Integer> missions;
	private Set<Integer> players;
	private Set<Integer> kills;
	
	public GameAddDTO(String gameName, String description, Integer gameState,
	                  double nwLat, double nwLng, double seLat, double seLng) {
		this.gameName = gameName;
		this.description = description;
		this.gameState = gameState;
		this.nwLat = nwLat;
		this.nwLng = nwLng;
		this.seLat = seLat;
		this.seLng = seLng;
	}
}
