package com.hvz.hvzapi.models.dtos.chat;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class ChatMessageAddDTO {
	private String message;
	private boolean isZombieGlobal;
	private boolean isHumanGlobal;
	private int senderId;
	private int gameId;
}
