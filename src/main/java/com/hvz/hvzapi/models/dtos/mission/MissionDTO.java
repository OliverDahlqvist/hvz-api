package com.hvz.hvzapi.models.dtos.mission;

import lombok.Data;

@Data
public class MissionDTO {
    private int id;
    private String name;
    private double lat;
    private double lng;
    private boolean visibleByHuman;
    private boolean visibleByZombie;
    private Integer gameId;
}
