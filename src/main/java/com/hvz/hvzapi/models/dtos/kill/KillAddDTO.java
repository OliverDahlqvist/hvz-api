package com.hvz.hvzapi.models.dtos.kill;

import lombok.Data;

@Data
public class KillAddDTO {
	private String story;
	private double lat;
	private double lng;
	private Integer killerId;
	private Integer victimId;
	private Integer gameId;
}
