package com.hvz.hvzapi.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

@Entity(name = "chat")
@Getter
@Setter
public class ChatMessage {
	@Id
	@GeneratedValue
	private int id;
	
	private String message;
	
	private boolean isZombieGlobal;
	private boolean isHumanGlobal;
	
	private Timestamp messageTime;
	
	@ManyToOne
	private Player sender;
	
	@ManyToOne
	private Game game;
}
