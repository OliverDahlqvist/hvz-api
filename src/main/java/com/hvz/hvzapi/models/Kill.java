package com.hvz.hvzapi.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Getter
@Setter
public class Kill {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private Timestamp killTime;
	
	private String story;
	
	@Column(name="kill_lat", nullable = false)
	private double lat;
	
	@Column(name = "kill_lng", nullable = false)
	private double lng;
	
	@ManyToOne
	private Player killer;
	
	@OneToOne
	private Player victim;
	
	@ManyToOne
	private Game game;
}
