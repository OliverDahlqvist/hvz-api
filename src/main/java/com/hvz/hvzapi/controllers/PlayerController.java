package com.hvz.hvzapi.controllers;

import com.hvz.hvzapi.exceptions.GameNotFoundException;
import com.hvz.hvzapi.mappers.PlayerMapper;

import com.hvz.hvzapi.models.Player;
import com.hvz.hvzapi.models.dtos.player.*;
import com.hvz.hvzapi.services.game.GameService;
import com.hvz.hvzapi.services.player.PlayerService;
import com.hvz.hvzapi.services.user.UserService;
import com.hvz.hvzapi.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.*;

@RestController
@CrossOrigin(origins = {"http://localhost:3000", "https://experis-react-hvz-frontend.herokuapp.com/"})
@RequestMapping(path = "api/v1")
public class PlayerController {
	private final PlayerService playerService;
	private final PlayerMapper playerMapper;
	
	private final GameService gameService;
	private final UserService userService;
	
	public PlayerController(PlayerService playerService, PlayerMapper playerMapper,
	                        GameService gameService, UserService userService) {
		this.playerService = playerService;
		this.playerMapper = playerMapper;
		this.gameService = gameService;
		this.userService = userService;
	}
	
	@Operation(summary = "Get all players (admin)")
	@ApiResponses({
			@ApiResponse(responseCode = "200",
					description = "OK",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = PlayerDTO.class)
					)
			),
			@ApiResponse(responseCode = "401",
					description = "This operation requires elevation",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			)
	})
	@PreAuthorize("hasRole('Admin')")
	@GetMapping("/players")
	public ResponseEntity findAll() {
		return ResponseEntity.ok(playerMapper.playerToPlayerDto(playerService.findAll()));
	}
	
	@Operation(summary = "Find player by id (admin)")
	@ApiResponses({
			@ApiResponse(responseCode = "200",
					description = "OK",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = PlayerDTO.class)
					)
			),
			@ApiResponse(responseCode = "401",
					description = "This operation requires elevation",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "404",
					description = "Player not found with supplied id",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					))
	})
	@PreAuthorize("hasRole('Admin')")
	@GetMapping("/players/{id}")
	public ResponseEntity findById(@PathVariable int id) {
		return ResponseEntity.ok(playerMapper.playerToPlayerDto(playerService.findById(id)));
	}

	@Operation(summary = "Gets the users current player in given game")
	@ApiResponses({
			@ApiResponse(responseCode = "200",
					description = "OK",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = PlayerDTO.class)
					)
			)
	})
	@GetMapping("/games/{game_id}/players/current")
	public ResponseEntity getUsersPlayerInGame(@PathVariable(name = "game_id") int gameId, @AuthenticationPrincipal Jwt jwt) {
		String uid = jwt.getClaimAsString("sub");
		return ResponseEntity.ok(playerMapper.playerToPlayerDto(playerService.findByGame_IdAndHvzUser_Uid(gameId, uid)));
	}

	@Operation(summary = "Add player to game")
	@ApiResponses({
			@ApiResponse(responseCode = "200",
					description = "OK",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = PlayerDTO.class)
					)
			),
			@ApiResponse(responseCode = "401",
					description = "This operation requires authorisation",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "404",
					description = "Game not found",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "400",
					description = "Mismatching user id (provided token and body)",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			)
	})
	@PostMapping("/games/{game_id}/players")
	public ResponseEntity addPlayer(@PathVariable(name = "game_id") int gameId,
	                                @RequestBody PlayerUserAddDTO playerAddDto,
	                                @AuthenticationPrincipal Jwt jwt) {

		try {
			var game = gameService.findById(gameId);
		
			// ensure user has no player in game already
			if (game.getPlayers().stream().anyMatch(p -> p.getHvzUser().getUid().contentEquals(jwt.getClaimAsString("sub"))))
				return ResponseEntity.badRequest().build();
		
			var player = playerMapper.addPlayerDtoToPlayer(playerAddDto);
			
			player.setBiteCode(UUID.randomUUID().toString());
			player.setGame(game);
			player.setHvzUser(userService.findById(jwt.getClaimAsString("sub")));
			
			playerService.add(player);
			
			URI uri = URI.create("players/" + player.getId());
			
			return ResponseEntity.created(uri).build();
		}
		catch (GameNotFoundException e) {
			return ResponseEntity.notFound().build();
		}
	}
	
	@Operation(summary = "Update player by id (admin)")
	@ApiResponses({
			@ApiResponse(responseCode = "204", description = "Player successfully updated"),
			@ApiResponse(responseCode = "401",
					description = "This operation requires elevation",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			)
	})
	@PutMapping("/players/{id}")
	public ResponseEntity updatePlayer(@RequestBody PlayerDTO dto, @PathVariable int id) {
		if (dto.getId() != id)
			return ResponseEntity.badRequest().build();
		
		playerService.update(playerMapper.playerDtoToPlayer(dto));
		return ResponseEntity.noContent().build();
	}
	
	@Operation(summary = "Delete player by id (admin)")
	@DeleteMapping("/players/{id}")
	@PreAuthorize("hasRole('Admin')")
	public ResponseEntity deletePlayer(@PathVariable int id) {
		playerService.deleteById(id);
		return ResponseEntity.noContent().build();
	}
	
	@Operation(summary = "Get all players in game")
	@ApiResponses({
			@ApiResponse(responseCode = "404",
					description = "There is no game with supplied id",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "401",
					description = "This operation requires authorisation",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "200",
					description = "OK",
					content = {
							@Content(
									mediaType = "application/json",
									schema = @Schema(implementation = PlayerDTO.class)
							),
							@Content(
									mediaType = "application/json",
									schema = @Schema(implementation = PlayerNonSensitiveDTO.class)
							)
					}
			)
	})
	@GetMapping("/games/{game_id}/players")
	public ResponseEntity getPlayers(@PathVariable(name = "game_id") int id,
	                                 @AuthenticationPrincipal Jwt jwt) {
		
		try {
			var game = gameService.findById(id);
			var players = game.getPlayers();
			
			return ResponseEntity.ok(
					jwt.getClaimAsStringList("roles").contains("Admin")
							? playerMapper.playerToPlayerDto(players)
							: playerMapper.playerToNonSensitiveDTO(players)
			);
		}
		catch (GameNotFoundException e) {
			return ResponseEntity.notFound().build();
		}
	}
	
	@Operation(summary = "Get player by id in game")
	@ApiResponses({
			@ApiResponse(responseCode = "404",
					description = "Player or game not found",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "200",
					description = "OK",
					content = {
							@Content(
									mediaType = "application/json",
									schema = @Schema(implementation = PlayerDTO.class)
							),
							@Content(
									mediaType = "application/json",
									schema = @Schema(implementation = PlayerNonSensitiveDTO.class)
							)
					}
			),
			@ApiResponse(responseCode = "401",
					description = "This operation requires authorisation",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "400",
					description = "Player not found in game",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			)
	})
	@GetMapping("/games/{game_id}/players/{player_id}")
	public ResponseEntity getPlayer(@PathVariable(name = "game_id") int gameId,
	                                @PathVariable(name = "player_id") int playerId,
	                                @AuthenticationPrincipal Jwt jwt) {
		
		try {
			var game = gameService.findById(gameId);
		
			var player = game.getPlayers().stream().filter(p -> p.getId() == playerId).findFirst();
			if (player.isEmpty()) return ResponseEntity.badRequest().build();
			
			return ResponseEntity.ok(
					jwt.getClaimAsStringList("roles").contains("Admin")
							? playerMapper.playerToPlayerDto(player.get())
							: playerMapper.playerToNonSensitiveDTO(player.get())
			);
		}
		catch (GameNotFoundException e) {
			return ResponseEntity.notFound().build();
		}
	}
	
	@Operation(summary = "Update player zombie status")
	@ApiResponses({
			@ApiResponse(responseCode = "204", description = "Successfully updated player"),
			@ApiResponse(responseCode = "401",
					description = "This operation requires authentication",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "404",
					description = "Game or player not found",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			)
	})
	@PatchMapping("/games/{game_id}/players/{player_id}")
	public ResponseEntity updatePlayer(@PathVariable(name = "game_id") int gameId,
	                                   @PathVariable(name = "player_id") int playerId,
	                                   @RequestBody PlayerEditDTO editDto,
	                                   @AuthenticationPrincipal Jwt jwt) {
		
		/*
			Edit dto id must match supplied player id
			
			One of the following must also be true:
				User is admin
				User owns the player (in game with supplied game id) who is the killer of player with supplied player id
		 */
		if (editDto.getId() != playerId || !jwt.getClaimAsStringList("roles").contains("Admin"))
			return ResponseEntity.badRequest().build();
		
		try {
			var game = gameService.findById(gameId);
			var playerFind = game.getPlayers().stream().filter(p -> p.getId() == playerId).findAny();
			
			if (playerFind.isEmpty())
				return ResponseEntity.notFound().build();
			
			var player = playerFind.get();
			player.setHuman(editDto.isHuman());
			
			playerService.update(player);
			
			return ResponseEntity.noContent().build();
		}
		catch (GameNotFoundException e) {
			return ResponseEntity.notFound().build();
		}
	}
	
	@Operation(summary = "Delete player from game")
	@ApiResponses({
			@ApiResponse(responseCode = "204", description = "Player successfully deleted"),
			@ApiResponse(responseCode = "401",
					description = "This operation requires elevation",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "404",
					description = "Game not found",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "400",
					description = "Player not found or player not in game",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					))
	})
	@DeleteMapping("/games/{game_id}/players/{player_id}")
	@PreAuthorize("hasRole('Admin')")
	public ResponseEntity deletePlayerById(@PathVariable(name = "game_id") int gameId,
	                                       @PathVariable(name = "player_id") int playerId) {
		
		try {
			var game = gameService.findById(gameId);
			
			// ensure player actually is in the selected game
			if (game.getPlayers().stream().noneMatch(p -> p.getId() == playerId))
				return ResponseEntity.badRequest().build();
			
			playerService.deleteById(playerId);
			return ResponseEntity.noContent().build();
		}
		catch (GameNotFoundException e) {
			return ResponseEntity.notFound().build();
		}
	}
	
	@Operation(summary = "Update some amount of players")
	@ApiResponse(responseCode = "200",
			description = "OK",
			content = @Content(mediaType = "text/plain",
					examples = @ExampleObject(name = "", value = "Updated 4")
			)
	)
	@PreAuthorize("hasRole('Admin')")
	@PutMapping("/players")
	public ResponseEntity updateAll(@RequestBody PlayerDTO[] dtos) {
		int updatedCount = 0;
		
		for (var dto: dtos) {
			try {
				var player = playerService.findById(dto.getId());
				var newPlayer = playerMapper.playerDtoToPlayer(dto);
				
				if (!player.equals(newPlayer)) {
					playerService.update(newPlayer);
					updatedCount++;
				}
			}
			catch (Exception ignored) { }
		}
		
		return ResponseEntity.ok("Updated " + updatedCount);
	}
}
