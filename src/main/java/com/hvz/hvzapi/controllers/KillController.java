package com.hvz.hvzapi.controllers;

import com.hvz.hvzapi.exceptions.GameNotFoundException;
import com.hvz.hvzapi.exceptions.KillNotFoundException;
import com.hvz.hvzapi.exceptions.PlayerNotFoundException;
import com.hvz.hvzapi.mappers.KillMapper;
import com.hvz.hvzapi.models.Kill;
import com.hvz.hvzapi.models.dtos.kill.KillAddByBiteCodeDTO;
import com.hvz.hvzapi.models.dtos.kill.KillAddDTO;
import com.hvz.hvzapi.models.dtos.kill.KillDTO;
import com.hvz.hvzapi.services.game.GameService;
import com.hvz.hvzapi.services.kill.KillService;
import com.hvz.hvzapi.services.player.PlayerService;
import com.hvz.hvzapi.services.user.UserService;
import com.hvz.hvzapi.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.sql.Timestamp;
import java.util.UUID;

@RestController
@CrossOrigin(origins = {"http://localhost:3000", "https://experis-react-hvz-frontend.herokuapp.com/"})
@RequestMapping(path = "api/v1")
public class KillController {
	private final KillService killService;
	private final KillMapper killMapper;
	
	private final GameService gameService;
	private final UserService userService;
	
	private final PlayerService playerService;
	
	public KillController(KillService killService, KillMapper killMapper,
	                      GameService gameService, UserService userService,
	                      PlayerService playerService) {
		
		this.killService = killService;
		this.killMapper = killMapper;
		this.gameService = gameService;
		this.userService = userService;
		this.playerService = playerService;
	}
	
	private Kill addDtoToKill(KillAddDTO dto) {
		var kill = killMapper.killAddDtoToKill(dto);
		kill.setKillTime(new Timestamp(System.currentTimeMillis()));
		
		return kill;
	}
	
	private Kill addDtoToKill(KillAddByBiteCodeDTO dto) {
		var kill = killMapper.killAddByBiteCodeToKill(dto);
		kill.setKillTime(new Timestamp(System.currentTimeMillis()));
		
		return kill;
	}
	
	//region Getters
	@Operation(summary = "Find all kills (admin)")
	@ApiResponses({
			@ApiResponse(responseCode = "401",
					description = "This operation requires elevation",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "200",
					description = "OK",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = KillDTO.class)
					)
			)
	})
	@PreAuthorize("hasRole('Admin')")
	@GetMapping("/kills")
	public ResponseEntity findAll() {
		return ResponseEntity.ok(killMapper.killToKillDto(killService.findAll()));
	}
	
	@Operation(summary = "Get kill by id (admin)")
	@ApiResponses({
			@ApiResponse(responseCode = "401",
					description = "This operation requires elevation",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "200",
					description = "OK",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = KillDTO.class)
					)
			),
			@ApiResponse(responseCode = "404",
					description = "Kill not found",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			)
	})
	@GetMapping("/kills/{id}")
	@PreAuthorize("hasRole('Admin')")
	public ResponseEntity findById(@PathVariable int id) {
		return ResponseEntity.ok(killMapper.killToKillDto(killService.findById(id)));
	}
	
	@Operation(summary = "Get all kills in game")
	@ApiResponses({
			@ApiResponse(responseCode = "401",
					description = "This operation requires authentication",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "400",
					description = "User must either be admin or own a player in the game",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "200",
					description = "OK",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = KillDTO.class)
					)
			),
			@ApiResponse(responseCode = "404",
					description = "Game not found",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			)
	})
	@GetMapping("/games/{game_id}/kills")
	public ResponseEntity getGameKills(@PathVariable(name = "game_id") int gameId,
	                                   @AuthenticationPrincipal Jwt jwt) {
		/*
			If there is no player the user must be admin
		 */
		if (!jwt.getClaimAsStringList("roles").contains("Admin") && userService.findById(
				jwt.getClaimAsString("sub"))
				    .getPlayers().stream()
				    .noneMatch(p -> p.getGame().getId() == gameId))
			return ResponseEntity.badRequest().build();
		
		try {
			var game = gameService.findById(gameId);
			return ResponseEntity.ok(killMapper.killToKillDto(game.getKills()));
		}
		catch (GameNotFoundException e) {
			return ResponseEntity.notFound().build();
		}
	}
	
	@Operation(summary="Get kill by id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "401",
					description = "This operation requires authentication",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "400",
					description = "User must either be admin or own a player referenced in kill",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "200",
					description = "OK",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = KillDTO.class)
					)
			),
			@ApiResponse(responseCode = "404",
					description = "OK",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			)
	})
	@GetMapping("/games/{game_id}/kills/{kill_id}")
	public ResponseEntity getGameKillById(@PathVariable(name = "game_id") int gameId,
	                                      @PathVariable(name = "kill_id") int killId,
	                                      @AuthenticationPrincipal Jwt jwt) {
		
		try {
			var game = gameService.findById(gameId);
			var kill = killService.findById(killId);
			
			if (!jwt.getClaimAsStringList("roles").contains("Admin")
					    || userService.findById(jwt.getClaimAsString("sub"))
							       .getPlayers().stream()
							       .anyMatch(p -> (p.getGame().getId() == gameId) ||
									                      (p.getId() == kill.getKiller().getId() || p.getId() == kill.getVictim().getId())))
				return ResponseEntity.badRequest().build();
			
			return ResponseEntity.ok(killMapper.killToKillDto(kill));
		}
		catch (GameNotFoundException | KillNotFoundException e) {
			return ResponseEntity.notFound().build();
		}
	}
	//endregion
	
	//region Posts
	@Operation(summary = "Add a new kill (admin)")
	@ApiResponses({
			@ApiResponse(responseCode = "401",
					description = "This operation requires elevation",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "201", description = "Successfully added kill")
	})
	@PreAuthorize("hasRole('Admin')")
	@PostMapping("/kills")
	public ResponseEntity addKill(@RequestBody KillAddDTO dto) {
		var kill = addDtoToKill(dto);
		
		killService.add(kill);
		
		URI uri = URI.create("kills/" + kill.getId());
		
		return ResponseEntity.created(uri).build();
	}
	
	@Operation(summary = "Get kills in game by id")
	@ApiResponses({
			@ApiResponse(responseCode = "401",
					description = "This operation requires authentication",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "400",
					description = "Mismatching game id; user not admin; user does not have a player in the game or victim is not human",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "404",
					description = "Game or victim not found",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "201", description = "Successfully added new kill to game")
	})
	@PostMapping("/games/{game_id}/kills")
	public ResponseEntity addKillToGame(@PathVariable(name = "game_id") int gameId,
	                                    @RequestBody KillAddByBiteCodeDTO dto,
	                                    @AuthenticationPrincipal Jwt jwt) {
		/*
			One of the following must be true:
				User owns the player who performs the kill
				User is admin
		 */
		
		try {
			var game = gameService.findById(gameId);
			
			var killer = userService.findById(
							jwt.getClaimAsString("sub")).getPlayers()
					             .stream().filter(p -> p.getGame().getId() == gameId)
					             .findAny();
			
			if (killer.isEmpty()) return ResponseEntity.badRequest().build();
			
			var victimCode = UUID.fromString(dto.getBiteCode());
			var victim = playerService.findByBiteCode(victimCode.toString());
			
			if (!victim.isHuman())
				return ResponseEntity.badRequest().build();
			
			victim.setHuman(false);
			playerService.update(victim);
			
			var kill = addDtoToKill(dto);
			kill.setKiller(killer.get());
			kill.setGame(game);
			
			killService.add(kill);
			
			URI uri = URI.create("kills/" + kill.getId());
			return ResponseEntity.created(uri).build();
			
		}
		catch (GameNotFoundException | PlayerNotFoundException e) {
			return ResponseEntity.notFound().build();
		}
		catch (IllegalArgumentException e) {
			return ResponseEntity.badRequest().build();
		}
	}
	//endregion
	
	//region Puts
	@Operation(summary = "Update a kill in game with id")
	@ApiResponses({
			@ApiResponse(responseCode = "200",
					description = "OK",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = KillDTO.class)
					)
			),
			@ApiResponse(responseCode = "404",
					description = "Game or kill not found",
					content = @Content(mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "401",
					description = "This operation requires authentication",
					content = @Content(mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			)
	})
	@PutMapping("/games/{game_id}/kills/{kill_id}")
	public ResponseEntity updateKillInGame(@PathVariable(name = "game_id") int gameId,
	                                       @PathVariable(name = "kill_id") int killId,
	                                       @RequestBody KillDTO dto,
	                                       @AuthenticationPrincipal Jwt jwt) {
		
		try {
			var game = gameService.findById(gameId);
			
			/*
			All the following must be true:
				Kill must exist in game
				DTO kill id must match supplied kill id
				DTO game id must match supplied game id
				User must either be admin or own killer player
		    */
			if (game.getKills().stream().noneMatch(k -> k.getId() == killId)
					    || dto.getId() != killId
					    || dto.getGameId() != gameId
					    || (!jwt.getClaimAsStringList("roles").contains("Admin")
							        && userService.findById(jwt.getClaimAsString("sub"))
									           .getPlayers().stream()
									           .noneMatch(p -> p.getId() == dto.getKillerId())))
				return ResponseEntity.badRequest().build();
		}
		catch (GameNotFoundException e) {
			return ResponseEntity.notFound().build();
		}
		
		killService.update(killMapper.killDtoToKill(dto));
		return ResponseEntity.noContent().build();
	}
	//endregion
	
	//region Deletes
	
	@Operation(summary = "Delete kill by id")
	@ApiResponses({
			@ApiResponse(responseCode = "204", description = "Successfully removed kill"),
			@ApiResponse(responseCode = "401",
					description = "This operation requires elevation",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			)
	})
	@PreAuthorize("hasRole('Admin')")
	@DeleteMapping("/kills/{id}")
	public ResponseEntity deleteKill(@PathVariable int id) {
		killService.deleteById(id);
		return ResponseEntity.noContent().build();
	}
	
	@Operation(summary = "Delete kill by id")
	@ApiResponses({
			@ApiResponse(responseCode = "204", description = "Successfully removed kill"),
			@ApiResponse(responseCode = "401",
					description = "This operation requires elevation",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "400",
					description = "No kill with supplied id exists in game with supplied id",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "404",
					description = "Game not found with supplied id",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			)
	})
	@PreAuthorize("hasRole('Admin')")
	@DeleteMapping("/games/{game_id}/kills/{kill_id}")
	public ResponseEntity deleteKill(@PathVariable(name = "game_id") int gameId,
	                                 @PathVariable(name = "kill_id") int killId) {
		
		try {
			var game = gameService.findById(gameId);
			
			if (game.getKills().stream().noneMatch(k -> k.getId() == killId))
				return ResponseEntity.badRequest().build();
			
			killService.deleteById(killId);
			
			return ResponseEntity.noContent().build();
		}
		catch (GameNotFoundException e) {
			return ResponseEntity.notFound().build();
		}
	}
	//endregion
}
