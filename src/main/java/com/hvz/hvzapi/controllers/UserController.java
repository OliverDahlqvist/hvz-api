package com.hvz.hvzapi.controllers;

import com.hvz.hvzapi.mappers.UserMapper;
import com.hvz.hvzapi.models.User;
import com.hvz.hvzapi.models.dtos.user.UserDTO;
import com.hvz.hvzapi.services.user.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin(origins = {"http://localhost:3000", "https://experis-react-hvz-frontend.herokuapp.com/"})
@RequestMapping(path = "api/v1/users")
public class UserController {
	private final UserService userService;
	private final UserMapper userMapper;
	
	public UserController(UserService userService, UserMapper userMapper) {
		this.userService = userService;
		this.userMapper = userMapper;
	}

	@GetMapping("/info")
	public ResponseEntity getLoggedInUserInfo(@AuthenticationPrincipal Jwt principal) {
		Map<String, String> map = new HashMap<>();
		map.put("subject", principal.getClaimAsString("sub"));
		map.put("user_name", principal.getClaimAsString("preferred_username"));
		map.put("email", principal.getClaimAsString("email"));
		map.put("first_name", principal.getClaimAsString("given_name"));
		map.put("last_name", principal.getClaimAsString("family_name"));
		map.put("roles", String.valueOf(principal.getClaimAsStringList("roles")));
		return ResponseEntity.ok(map);
	}

	@GetMapping("/current")
	public ResponseEntity getCurrentlyLoggedInUser(@AuthenticationPrincipal Jwt jwt) {
		return ResponseEntity.ok(
				userMapper.userToUserDto(userService.findById(jwt.getClaimAsString("sub")))
		);
	}

	@PostMapping("/register")
	public ResponseEntity addNewUserFromJwt(@AuthenticationPrincipal Jwt jwt) {
		User user = userService.add(jwt.getClaimAsString("sub"), jwt.getClaimAsString("given_name"), jwt.getClaimAsString("family_name"));
		System.out.println(jwt.getClaimAsString("roles"));
		URI uri = URI.create("api/v1/users/" + user.getUid());
		return ResponseEntity.created(uri).build();
	}

	@GetMapping
	public ResponseEntity findAll() {
		return ResponseEntity.ok(userMapper.userToUserDto(userService.findAll()));
	}
}
