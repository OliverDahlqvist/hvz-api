package com.hvz.hvzapi.controllers;

import com.hvz.hvzapi.mappers.GameMapper;
import com.hvz.hvzapi.models.Game;
import com.hvz.hvzapi.models.dtos.game.GameAddDTO;
import com.hvz.hvzapi.models.dtos.game.GameDTO;
import com.hvz.hvzapi.services.game.GameService;
import com.hvz.hvzapi.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@CrossOrigin(origins = {"http://localhost:3000", "https://experis-react-hvz-frontend.herokuapp.com/"})
@RequestMapping(path = "api/v1/games")
public class GameController {
    private final GameService gameService;
    private final GameMapper gameMapper;

    public GameController(GameService gameService, GameMapper gameMapper) {
        this.gameService = gameService;
        this.gameMapper = gameMapper;
    }
    
    //region Getters
    @Operation(summary = "Get all games")
    @ApiResponse(responseCode = "200",
            description = "Success",
            content = @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = GameDTO.class)
            )
    )
    @GetMapping
    public ResponseEntity<Collection<GameDTO>> findAll() {
        return ResponseEntity.ok(gameMapper.gamesToGameDTOs(gameService.findAll()));
    }
    
    @Operation(summary = "Get a game by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "OK",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = GameDTO.class)
                    )
            ),
            @ApiResponse(responseCode = "404",
                    description = "Game does not exist with supplied ID",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)
                    )
            )
    })
    @GetMapping("/{id}")
    public ResponseEntity<GameDTO> findById(@PathVariable int id){
        return ResponseEntity.ok(gameMapper.gameToGameDTO(gameService.findById(id)));
    }
    //endregion
    //region Puts
    @Operation(summary = "Updates a game")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "Game successfully updated"),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content( mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)
                    )
            ),
            @ApiResponse(responseCode = "404",
                    description = "Game not found with supplied ID",
                    content = @Content( mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)
                    )
            ),
            @ApiResponse(responseCode = "401",
                    description = "This operation requires elevation",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)
                    )
            )
    })
    @PreAuthorize("hasRole('Admin')")
    @PutMapping(value = "/{id}")
    public ResponseEntity update(@RequestBody GameDTO gameDTO, @PathVariable int id){
        if(gameDTO.getId() != id)
            return ResponseEntity.badRequest().build();
        
        gameService.update(gameMapper.gameDTOToGame(gameDTO));
        return ResponseEntity.noContent().build();
    }
    //endregion
    //region Posts
    @Operation(summary = "Adds a game")
    @ApiResponses({
            @ApiResponse(responseCode = "201",
                    description = "Created",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = GameDTO.class)
                    )
            ),
            @ApiResponse(responseCode = "400",
                    description = "Invalid ID supplied",
                    content = @Content( mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)
                    )
            ),
            @ApiResponse(responseCode = "401",
                    description = "This operation requires elevation",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)
                    )
            )
    })
    @PreAuthorize("hasRole('Admin')")
    @PostMapping
    public ResponseEntity add(@RequestBody GameAddDTO game){
        Game addedGame = gameMapper.gameAddDtoToGame(game);
        
        gameService.add(addedGame);
        
        URI uri = URI.create("games/" + addedGame.getId());
        return ResponseEntity.created(uri).build();
    }
    //endregion
    @Operation(summary = "Deletes a game")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "Game successfully deleted"),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)
                    )
            ),
            @ApiResponse(responseCode = "401",
                    description = "This operation requires elevation",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)
                    )
            )
    })
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity delete(@PathVariable int id){
        gameService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
