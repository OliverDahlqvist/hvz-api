package com.hvz.hvzapi.controllers;

import com.hvz.hvzapi.mappers.ChatMapper;
import com.hvz.hvzapi.models.dtos.chat.ChatMessageAddDTO;
import com.hvz.hvzapi.models.dtos.chat.ChatMessageDTO;
import com.hvz.hvzapi.services.chat.ChatService;
import com.hvz.hvzapi.services.game.GameService;
import com.hvz.hvzapi.services.user.UserService;
import com.hvz.hvzapi.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.sql.Timestamp;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = {"http://localhost:3000", "https://experis-react-hvz-frontend.herokuapp.com/"})
@RequestMapping(path = "api/v1")
public class ChatController {
	private final ChatService chatService;
	private final ChatMapper chatMapper;
	
	private final GameService gameService;
	private final UserService userService;
	
	public ChatController(ChatService chatService, ChatMapper chatMapper,
	                      GameService gameService, UserService userService) {
		
		this.chatService = chatService;
		this.chatMapper = chatMapper;
		this.gameService = gameService;
		this.userService = userService;
	}
	
	@GetMapping("/chat")
	@PreAuthorize("hasRole('Admin')")
	@Operation(summary = "Get all messages in all games (admin only)")
	@ApiResponses({
			@ApiResponse(responseCode = "401",
					description = "This operation requires elevation",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "200",
					description = "OK",
					content = @Content(mediaType = "application/json",
							schema = @Schema(implementation = ChatMessageDTO.class)
					)
			)
	})
	public ResponseEntity findAll() {
		return ResponseEntity.ok(chatMapper.chatMessageToDto(chatService.findAll()));
	}
	
	@GetMapping("/chat/{id}")
	@PreAuthorize("hasRole('Admin')")
	@Operation(summary = "Find chat message by id (admin only)")
	@ApiResponses({
			@ApiResponse(responseCode = "401",
					description = "This operation requires elevation",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "200",
					description = "OK",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ChatMessageDTO.class)
					)
			)
	})
	public ResponseEntity findById(@PathVariable int id) {
		return ResponseEntity.ok(chatMapper.chatMessageToDto(chatService.findById(id)));
	}
	
	@DeleteMapping("/chat/{id}")
	@PreAuthorize("hasRole('Admin')")
	@Operation(summary = "Delete chat message by id (admin only)")
	@ApiResponses({
			@ApiResponse(responseCode = "401",
					description = "This operation requires elevation",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "204", description = "Successfully removed message")
	})
	public ResponseEntity deleteChatMessage(@PathVariable int id) {
		chatService.deleteById(id);
		return ResponseEntity.noContent().build();
	}
	
	@Operation(summary = "Get all chat messages of game")
	@ApiResponses({
			@ApiResponse(responseCode = "401",
					description = "This operation requires authentication",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "404",
						  description = "Game not found with supplied id",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "200",
					description = "OK",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ChatMessageDTO.class)
					)
			),
			@ApiResponse(responseCode = "400", description = "User has no player in game, or user is not admin")
	})
	@GetMapping(value = "/games/{game_id}/chat")
	public ResponseEntity getGameChat(@PathVariable(name = "game_id") int id,
	                                  @AuthenticationPrincipal Jwt jwt) {
		
		var game = gameService.findById(id);
		
		if (game == null)
			return ResponseEntity.notFound().build();
		
		var user = userService.findById(jwt.getClaimAsString("sub"));
		
		/*
			Find player of user that is in the game
		 */
		var player = user.getPlayers().stream()
				             .filter(p -> p.getGame().getId() == id)
				             .findAny();
		
		/*
			If there is no player the user must be admin
		 */
		if (player.isEmpty() || !jwt.getClaimAsStringList("roles").contains("Admin"))
			return ResponseEntity.badRequest().build();
		
		return ResponseEntity.ok(
				chatMapper.chatMessageToDto(
						jwt.getClaimAsStringList("roles").contains("Admin")
								? game.getMessages()                        // get all messages if admin
								: game.getMessages().stream()               // filter correct chat depending on player team
										  .filter(m -> m.isHumanGlobal() == player.get().isHuman() && m.isZombieGlobal() == !player.get().isHuman())
										  .collect(Collectors.toList())
				)
		);
	}
	
	@Operation(summary = "Add a new message to game")
	@ApiResponses({
			@ApiResponse(responseCode = "401",
					description = "This operation requires authentication",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "404",
					description = "Game not found with supplied id",
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "201", description = "Successfully added new message"),
			@ApiResponse(responseCode = "400",
					description = "User is not admin or does not own a player in the game (or malformed request)"
			)
	})
	@PostMapping("/games/{game_id}/chat")
	public ResponseEntity addMessage(@PathVariable(name = "game_id") int id,
	                                 @RequestBody ChatMessageAddDTO messageDto,
	                                 @AuthenticationPrincipal Jwt jwt) {
		
		var game = gameService.findById(id);
		
		if (game == null)
			return ResponseEntity.notFound().build();
		
		/*
			One of the following must be true:
				User is admin
				User owns the player who sent the message
				
			The game id in dto must also match supplied game id
		 */
		if (jwt.getClaimAsStringList("roles").contains("Admin")
				    || userService.findById(jwt.getClaimAsString("sub"))
						       .getPlayers().stream()
						       .noneMatch(p -> p.getId() == messageDto.getSenderId())
				    || messageDto.getGameId() != id)
			return ResponseEntity.badRequest().build();
		
		var message = chatMapper.chatAddDtoToMessage(messageDto);
		message.setMessageTime(new Timestamp(System.currentTimeMillis()));
		chatService.add(message);
		
		URI uri = URI.create("chat/" + message.getId());
		
		return ResponseEntity.created(uri).build();
	}
}
