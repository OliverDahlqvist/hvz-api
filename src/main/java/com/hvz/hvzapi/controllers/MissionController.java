package com.hvz.hvzapi.controllers;

import com.hvz.hvzapi.exceptions.GameNotFoundException;
import com.hvz.hvzapi.mappers.MissionMapper;
import com.hvz.hvzapi.models.dtos.game.GameDTO;
import com.hvz.hvzapi.models.dtos.mission.MissionAddDTO;
import com.hvz.hvzapi.models.dtos.mission.MissionDTO;
import com.hvz.hvzapi.services.game.GameService;
import com.hvz.hvzapi.services.mission.MissionService;
import com.hvz.hvzapi.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@CrossOrigin(origins = {"http://localhost:3000", "https://experis-react-hvz-frontend.herokuapp.com/"})
@RequestMapping(path = "api/v1")
public class MissionController {
    private final MissionService missionService;
    private final MissionMapper missionMapper;
    
    private final GameService gameService;

    public MissionController(MissionService missionService, MissionMapper missionMapper,
                             GameService gameService) {
        
        this.missionService = missionService;
        this.missionMapper = missionMapper;
        this.gameService = gameService;
    }

    @Operation(summary = "Get all games")
    @ApiResponses({
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = GameDTO.class)
                    )
            ),
            @ApiResponse(responseCode = "404",
                    description = "Missions does not exist with supplied ID",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)
                    )
            )
    })
    @GetMapping(value = "/games/{id}/missions")
    public ResponseEntity<Collection<MissionDTO>> findAll(@PathVariable int id){
        return ResponseEntity.ok(missionMapper.missionsToMissionDTOs(missionService.findAllByGameId(id)));
    }
    
    @Operation(summary = "Get a game by ID")
    @ApiResponses({
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = GameDTO.class)
                    )
            ),
            @ApiResponse(responseCode = "404",
                    description = "Game OR mission does not exist with supplied ids",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)
                    )
            )
    })
    @GetMapping(value = "/games/{gameId}/missions/{missionId}")
    public ResponseEntity<MissionDTO> findById(@PathVariable int gameId,
                                               @PathVariable int missionId){
        return ResponseEntity.ok(missionMapper.missionToMissionDTO(missionService.findByIdAndGameId(gameId, missionId)));
    }

    @Operation(summary = "Updates a mission")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204", description = "Mission successfully updated"),
            @ApiResponse(responseCode = "400",
                    description = "Mission does not belong to game with supplied id",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)
                    )
            ),
            @ApiResponse(responseCode = "404",
                    description = "Game or mission not found with supplied ids",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)
                    )
            ),
            @ApiResponse(responseCode = "401",
                    description = "This operation requires elevation",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)
                    )
            )
    })
    @PreAuthorize("hasRole('Admin')")
    @PutMapping(value = "/games/{gameId}/missions/{missionId}")
    public ResponseEntity update(@RequestBody MissionDTO missionDTO,
                                 @PathVariable int gameId,
                                 @PathVariable int missionId){
        
        try {
            var game = gameService.findById(gameId);
    
            if (game.getMissions().stream().noneMatch(m -> m.getId() == missionId))
                return ResponseEntity.badRequest().build();
    
            missionService.update(missionMapper.missionDTOToMission(missionDTO));
    
            return ResponseEntity.noContent().build();
        }
        catch (GameNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "Adds a mission to a specific game")
    @ApiResponses({
            @ApiResponse(responseCode = "201",
                    description = "Created",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = GameDTO.class)
                    )
            ),
            @ApiResponse(responseCode = "400",
                    description = "Invalid ID supplied",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)
                    )
            ),
            @ApiResponse(responseCode = "401",
                    description = "This operation requires elevation",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)
                    )
            )
    })
    @PreAuthorize("hasRole('Admin')")
    @PostMapping("/games/{gameId}/missions")
    public ResponseEntity add(@RequestBody MissionAddDTO dto, @PathVariable int gameId) {
        dto.setGameId(gameId);
        var mission = missionMapper.missionAddDTOToMission(dto);
        missionService.add(mission);
    
        URI uri = URI.create(gameId + "/" + mission.getId());
        return ResponseEntity.created(uri).build();
    }
    
    @Operation(summary = "Deletes a mission")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "Game successfully deleted"),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)
                    )
            ),
            @ApiResponse(responseCode = "404",
                    description = "Mission or game not found with supplied ID",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)
                    )
            )
    })
    @PreAuthorize("hasRole('Admin')")
    @DeleteMapping("/games/{gameId}/missions/{missionId}")
    public ResponseEntity delete(@PathVariable int gameId, @PathVariable int missionId){
        return missionService.deleteByIdAndGameId(missionId, gameId).build();
    }
}
