package com.hvz.hvzapi.mappers;

import com.hvz.hvzapi.models.Game;
import com.hvz.hvzapi.models.Kill;
import com.hvz.hvzapi.models.Mission;
import com.hvz.hvzapi.models.Player;
import com.hvz.hvzapi.models.dtos.game.GameAddDTO;
import com.hvz.hvzapi.models.dtos.game.GameDTO;
import com.hvz.hvzapi.services.game.GameService;
import com.hvz.hvzapi.services.kill.KillService;
import com.hvz.hvzapi.services.mission.MissionService;
import com.hvz.hvzapi.services.player.PlayerService;
import com.hvz.hvzapi.util.GameState;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class GameMapper {
    @Autowired
    protected GameService gameService;

    @Autowired
    protected MissionService missionService;

    @Autowired
    protected PlayerService playerService;
    
    @Autowired
    protected KillService killService;

    @Mappings({
            @Mapping(target = "missions", source = "missions", qualifiedByName = "missionsToIds"),
            @Mapping(target = "gameState", source = "gameState", qualifiedByName = "gameStateToId"),
            @Mapping(target = "players", source = "players", qualifiedByName = "playersToIds"),
            @Mapping(target = "kills", source = "kills", qualifiedByName = "killsToIds")
    })
    public abstract GameDTO gameToGameDTO(Game game);

    @Mappings({
            @Mapping(target = "missions", source = "missions", qualifiedByName = "missionIdsToMissions"),
            @Mapping(target = "gameState", source = "gameState", qualifiedByName = "gameStateIdToGameState"),
            @Mapping(target = "players", source = "players", qualifiedByName = "playerIdsToPlayers"),
            @Mapping(target = "kills", source = "kills", qualifiedByName = "idsToKills")
    })
    public abstract Game gameDTOToGame(GameDTO game);
    
    @Mappings({
            @Mapping(target = "missions", source = "missions", qualifiedByName = "missionIdsToMissions"),
            @Mapping(target = "gameState", source = "gameState", qualifiedByName = "gameStateIdToGameState"),
            @Mapping(target = "players", source = "players", qualifiedByName = "playerIdsToPlayers"),
            @Mapping(target = "kills", source = "kills", qualifiedByName = "idsToKills")
    })
    public abstract Game gameAddDtoToGame(GameAddDTO game);
    
    @Named("missionsToIds")
    Set<Integer> mapMissionsToIds(Set<Mission> missions){
        if(missions == null) return null;
        return missions.stream().map(Mission::getId).collect(Collectors.toSet());
    }

    @Named("missionIdsToMissions")
    Set<Mission> mapMissionIdsToMissions(Set<Integer> ids){
        if(ids == null) return null;
        return ids.stream().map(i -> missionService.findById(i)).collect(Collectors.toSet());
    }

    @Named("playersToIds")
    Set<Integer> mapPlayersToIds(Set<Player> players){
        if(players == null) return null;
        return players.stream().map(Player::getId).collect(Collectors.toSet());
    }

    @Named("playerIdsToPlayers")
    Set<Player> mapPlayerIdsToPlayers(Set<Integer> ids){
        if(ids == null) return null;
        return ids.stream().map(i -> playerService.findById(i)).collect(Collectors.toSet());
    }

    @Named("gameStateToId")
    Integer mapGameStateToId(GameState gameState){
        return gameState.ordinal();
    }

    @Named("gameStateIdToGameState")
    GameState mapGameStateToId(int gameState){
        return GameState.values()[gameState];
    }
    
    @Named("killsToIds")
    Set<Integer> mapKillsToIds(Set<Kill> kills) {
        if (kills == null) return null;
        return kills.stream().map(Kill::getId).collect(Collectors.toSet());
    }
    
    @Named("idsToKills")
    Set<Kill> mapIdsToKills(Set<Integer> ids) {
        if (ids == null) return null;
        return ids.stream().map(id -> killService.findById(id)).collect(Collectors.toSet());
    }

    public abstract Collection<GameDTO> gamesToGameDTOs(Collection<Game> games);
}
