package com.hvz.hvzapi.mappers;

import com.hvz.hvzapi.models.Game;
import com.hvz.hvzapi.models.Mission;
import com.hvz.hvzapi.models.dtos.mission.MissionAddDTO;
import com.hvz.hvzapi.models.dtos.mission.MissionDTO;
import com.hvz.hvzapi.services.game.GameService;
import com.hvz.hvzapi.services.mission.MissionService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class MissionMapper {
    @Autowired
    protected MissionService missionService;

    @Autowired
    protected GameService gameService;

    @Mapping(target = "gameId", source = "game", qualifiedByName = "gameToId")
    public abstract MissionDTO missionToMissionDTO(Mission mission);

    @Mapping(target = "game", source = "gameId", qualifiedByName = "idToGame")
    public abstract Mission missionDTOToMission(MissionDTO mission);

    @Mapping(target = "game", source = "gameId", qualifiedByName = "idToGame")
    public abstract Mission missionAddDTOToMission(MissionAddDTO mission);

    @Named("idToGame")
    Game mapGameIdtoGame(int id){
        return gameService.findById(id);
    }

    @Named("gameToId")
    Integer mapGameToGameId(Game game){
        return game.getId();
    }

    public abstract Collection<MissionDTO> missionsToMissionDTOs(Collection<Mission> missions);
}
