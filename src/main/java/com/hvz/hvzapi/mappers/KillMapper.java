package com.hvz.hvzapi.mappers;

import com.hvz.hvzapi.models.Game;
import com.hvz.hvzapi.models.Kill;
import com.hvz.hvzapi.models.Player;
import com.hvz.hvzapi.models.dtos.kill.KillAddByBiteCodeDTO;
import com.hvz.hvzapi.models.dtos.kill.KillAddDTO;
import com.hvz.hvzapi.models.dtos.kill.KillDTO;
import com.hvz.hvzapi.services.game.GameService;
import com.hvz.hvzapi.services.player.PlayerService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class KillMapper {
	@Autowired
	protected PlayerService playerService;
	
	@Autowired
	protected GameService gameService;
	
	@Mappings({
			@Mapping(target = "killer", source = "killerId", qualifiedByName = "idToPlayer"),
			@Mapping(target = "victim", source = "victimId", qualifiedByName = "idToPlayer"),
			@Mapping(target = "game", source = "gameId", qualifiedByName = "idToGame")
	})
	public abstract Kill killAddDtoToKill(KillAddDTO dto);
	
	@Mappings({
			@Mapping(target = "killerId", source = "killer", qualifiedByName = "playerToId"),
			@Mapping(target = "victimId", source = "victim", qualifiedByName = "playerToId"),
			@Mapping(target = "gameId", source = "game", qualifiedByName = "gameToId")
	})
	public abstract KillDTO killToKillDto(Kill kill);
	
	public abstract Collection<KillDTO> killToKillDto(Collection<Kill> kills);
	
	@Mappings({
			@Mapping(target = "killer", source = "killerId", qualifiedByName = "idToPlayer"),
			@Mapping(target = "victim", source = "victimId", qualifiedByName = "idToPlayer"),
			@Mapping(target = "game", source = "gameId", qualifiedByName = "idToGame")
	})
	public abstract Kill killDtoToKill(KillDTO dto);
	
	@Mapping(source = "biteCode", target = "victim", qualifiedByName = "biteCodeToPlayer")
	public abstract Kill killAddByBiteCodeToKill(KillAddByBiteCodeDTO dto);
	
	@Named("idToPlayer")
	public Player mapIdToPlayer(Integer id) {
		if (id == null) return null;
		return playerService.findById(id);
	}
	
	@Named("idToGame")
	public Game mapIdToGame(Integer id) {
		if (id == null) return null;
		return gameService.findById(id);
	}
	
	@Named("playerToId")
	public Integer mapPlayerToId(Player player) {
		if (player == null) return null;
		return player.getId();
	}
	
	@Named("gameToId")
	public Integer mapGameToId(Game game) {
		if (game == null) return null;
		return game.getId();
	}
	
	@Named("biteCodeToPlayer")
	public Player mapBiteCodeToPlayer(String biteCode) {
		if (biteCode == null) return null;
		
		return playerService.findByBiteCode(biteCode);
	}
}
