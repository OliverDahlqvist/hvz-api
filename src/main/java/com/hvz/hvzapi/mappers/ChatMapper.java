package com.hvz.hvzapi.mappers;

import com.hvz.hvzapi.models.ChatMessage;
import com.hvz.hvzapi.models.Game;
import com.hvz.hvzapi.models.Player;
import com.hvz.hvzapi.models.dtos.chat.ChatMessageAddDTO;
import com.hvz.hvzapi.models.dtos.chat.ChatMessageDTO;
import com.hvz.hvzapi.services.game.GameService;
import com.hvz.hvzapi.services.player.PlayerService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class ChatMapper {
	@Autowired
	protected PlayerService playerService;
	
	@Autowired
	protected GameService gameService;
	
	@Mappings({
			@Mapping(source = "sender", target = "senderId", qualifiedByName = "playerToId"),
			@Mapping(source = "game", target = "gameId", qualifiedByName = "gameToId")
	})
	public abstract ChatMessageDTO chatMessageToDto(ChatMessage chatMessage);
	public abstract Collection<ChatMessageDTO> chatMessageToDto(Collection<ChatMessage> messages);
	
	@Mappings({
			@Mapping(source = "senderId", target = "sender", qualifiedByName = "idToPlayer"),
			@Mapping(source = "gameId", target = "game", qualifiedByName = "idToGame")
	})
	public abstract ChatMessage chatDtoToMessage(ChatMessageDTO dto);
	
	@Mappings({
			@Mapping(source = "senderId", target = "sender", qualifiedByName = "idToPlayer"),
			@Mapping(source = "gameId", target = "game", qualifiedByName = "idToGame")
	})
	public abstract ChatMessage chatAddDtoToMessage(ChatMessageAddDTO dto);
	
	@Named("idToPlayer")
	public Player mapIdToPlayer(Integer id) {
		if (id == null) return null;
		return playerService.findById(id);
	}
	
	@Named("playerToId")
	public Integer playerToId(Player player) {
		if (player == null) return null;
		return player.getId();
	}
	
	@Named("idToGame")
	public Game mapIdToGame(Integer id) {
		if (id == null) return null;
		return gameService.findById(id);
	}
	
	@Named("gameToId")
	public Integer mapGameToId(Game game) {
		if (game == null) return null;
		return game.getId();
	}
}
