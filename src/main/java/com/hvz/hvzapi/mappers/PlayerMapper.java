package com.hvz.hvzapi.mappers;

import com.hvz.hvzapi.models.*;
import com.hvz.hvzapi.models.dtos.player.*;
import com.hvz.hvzapi.services.chat.ChatService;
import com.hvz.hvzapi.services.game.GameService;
import com.hvz.hvzapi.services.kill.KillService;
import com.hvz.hvzapi.services.user.UserService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class PlayerMapper {
	@Autowired
	protected GameService gameService;
	
	@Autowired
	protected UserService userService;
	
	@Autowired
	protected ChatService chatService;
	
	@Autowired
	protected KillService killService;

	@Mappings({
			@Mapping(source = "userId", target = "hvzUser", qualifiedByName = "idToUser"),
			@Mapping(source = "game", target = "game", qualifiedByName = "idToGame"),
			@Mapping(source = "messageIds", target = "messages", qualifiedByName = "idsToMessages"),
			@Mapping(source = "killIds", target = "kills", qualifiedByName = "idsToKills")
	})
	public abstract Player playerDtoToPlayer(PlayerDTO dto);

	@Mappings({
			@Mapping(source = "game", target = "game", qualifiedByName = "gameToId"),
			@Mapping(source = "hvzUser", target = "userId", qualifiedByName = "userToId"),
			@Mapping(source = "messages", target = "messageIds", qualifiedByName = "messagesToIds"),
			@Mapping(source = "kills", target = "killIds", qualifiedByName = "killsToIds"),
	})
	public abstract PlayerDTO playerToPlayerDto(Player player);
	
	public abstract Collection<PlayerDTO> playerToPlayerDto(Collection<Player> entities);
	
	@Mappings({
			@Mapping(source = "gameId", target = "game", qualifiedByName = "idToGame"),
			@Mapping(source = "userId", target = "hvzUser", qualifiedByName = "idToUser"),
			@Mapping(source = "messageIds", target = "messages", qualifiedByName = "idsToMessages"),
			@Mapping(source = "killIds", target = "kills", qualifiedByName = "idsToKills")
	})
	public abstract Player addPlayerDtoToPlayer(PlayerAddDTO dto);
	
	@Mapping(source = "human", target = "patientZero", qualifiedByName = "inverseHuman")
	public abstract Player addPlayerDtoToPlayer(PlayerUserAddDTO dto);
	
	@Mappings({
			@Mapping(source = "messages", target = "messages", qualifiedByName = "messagesToText"),
			@Mapping(source = "kills", target="killCount", qualifiedByName = "killsToCount")
	})
	public abstract PlayerNonSensitiveDTO playerToNonSensitiveDTO(Player player);
	public abstract Collection<PlayerNonSensitiveDTO> playerToNonSensitiveDTO(Collection<Player> player);
	
	@Named("idToUser")
	public User idToUser(String id) {
		if (id == null) return null;
		return userService.findById(id);
	}
	
	@Named("userToId")
	public String mapUserToId(User user) {
		if (user == null) return null;
		return user.getUid();
	}

	@Named("gameToId")
	public Integer mapGameToId(Game game) {
		if (game == null) return null;
		return game.getId();
	}

	@Named("idToGame")
	public Game mapIdToGame(int id){
		return gameService.findById(id);
	}
	
	@Named("idsToMessages")
	public Set<ChatMessage> mapIdsToMessages(Set<Integer> ids) {
		if (ids == null) return null;
		return ids.stream().map(id -> chatService.findById(id)).collect(Collectors.toSet());
	}
	
	@Named("messagesToIds")
	public Set<Integer> mapMessagesToIds(Set<ChatMessage> messages) {
		if (messages == null) return null;
		return messages.stream().map(ChatMessage::getId).collect(Collectors.toSet());
	}
	
	@Named("idsToKills")
	public Set<Kill> mapIdsToKills(Set<Integer> ids) {
		if (ids == null) return null;
		return ids.stream().map(id -> killService.findById(id)).collect(Collectors.toSet());
	}
	
	@Named("killsToIds")
	public Set<Integer> mapKillsToIds(Set<Kill> kills) {
		if (kills == null) return null;
		return kills.stream().map(Kill::getId).collect(Collectors.toSet());
	}
	
	@Named("killsToCount")
	public Integer mapKillsToCount(Set<Kill> kills) {
		if (kills == null) return null;
		return kills.size();
	}
	
	@Named("messagesToText")
	public Set<String> mapMessagesToText(Set<ChatMessage> messages) {
		return messages.stream().map(ChatMessage::getMessage).collect(Collectors.toSet());
	}
	
	@Named("inverseHuman")
	public boolean mapHumanToPatientZero(boolean isHuman) {
		return !isHuman;
	}
}
