package com.hvz.hvzapi.mappers;

import com.hvz.hvzapi.models.Player;
import com.hvz.hvzapi.models.User;
import com.hvz.hvzapi.models.dtos.user.UserDTO;
import com.hvz.hvzapi.services.player.PlayerService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class UserMapper {
	@Autowired
	protected PlayerService playerService;
	
	@Mapping(source = "playerIds", target = "players", qualifiedByName = "idsToPlayers")
	public abstract User userDtoToUser(UserDTO dto);
	
	@Mapping(source = "players", target = "playerIds", qualifiedByName = "playersToIds")
	public abstract UserDTO userToUserDto(User user);

	public abstract Collection<UserDTO> userToUserDto(Collection<User> users);
	
	@Named("playersToIds")
	public Set<Integer> mapPlayersToIds(Set<Player> players) {
		if (players == null) return null;
		return players.stream().map(Player::getId).collect(Collectors.toSet());
	}
	
	@Named("idsToPlayers")
	public Set<Player> mapIdsToPlayers(Set<Integer> playerIds) {
		if (playerIds == null) return null;
		return playerIds.stream().map(id -> playerService.findById(id)).collect(Collectors.toSet());
	}
}
