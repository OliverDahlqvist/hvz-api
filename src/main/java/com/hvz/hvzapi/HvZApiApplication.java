package com.hvz.hvzapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HvZApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(HvZApiApplication.class, args);
    }

}
