package com.hvz.hvzapi.repositories;

import com.hvz.hvzapi.models.Kill;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KillRepository extends JpaRepository<Kill, Integer> {
}
