package com.hvz.hvzapi.repositories;

import com.hvz.hvzapi.models.ChatMessage;
import com.hvz.hvzapi.models.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface GameRepository extends JpaRepository<Game, Integer> {
	@Query(value = """
    SELECT chat.* FROM chat
        JOIN game
        ON chat.id = game.game_id
        WHERE game_id = :gameId
        AND (is_human_global = :isHuman
            OR is_zombie_global = NOT :isHuman)
""", nativeQuery = true)
	Set<ChatMessage> getChat(@Param("isHuman") boolean isHuman, @Param("gameId") int gameId);
}
