package com.hvz.hvzapi.repositories;

import com.hvz.hvzapi.models.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Integer> {
	Player findByBiteCode(String biteCode);
	Player findByGame_IdAndHvzUser_Uid(int gameId, String userUId);
}
