package com.hvz.hvzapi.repositories;

import com.hvz.hvzapi.models.Game;
import com.hvz.hvzapi.models.Mission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface MissionRepository extends JpaRepository<Mission, Integer> {
    Collection<Mission> findAllByGameId(int id);
    Mission findByGame_IdAndId(int gameId, int missionId);
    void deleteAllByGame(Game game);
}
