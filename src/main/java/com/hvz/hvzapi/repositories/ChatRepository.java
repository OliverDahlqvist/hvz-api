package com.hvz.hvzapi.repositories;

import com.hvz.hvzapi.models.ChatMessage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChatRepository extends JpaRepository<ChatMessage, Integer> {
}
