package com.hvz.hvzapi.exceptions;

public class ChatMessageNotFoundException extends RuntimeException {
	public ChatMessageNotFoundException(int id) {
		super("There is no chat message with id " + id);
	}
}
