package com.hvz.hvzapi.exceptions;

public class UserNotFoundException extends RuntimeException {
	public UserNotFoundException(String id) {
		super("No user found with id " + id);
	}
}
