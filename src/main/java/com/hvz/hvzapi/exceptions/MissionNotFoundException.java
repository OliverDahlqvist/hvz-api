package com.hvz.hvzapi.exceptions;

public class MissionNotFoundException extends RuntimeException {
    public MissionNotFoundException(int id) {
        super("Mission not found with ID: " + id);
    }
}
