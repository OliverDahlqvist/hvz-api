package com.hvz.hvzapi.exceptions;

public class KillNotFoundException extends RuntimeException {
	public KillNotFoundException(int id) {
		super("There is no kill with id " + id);
	}
}
