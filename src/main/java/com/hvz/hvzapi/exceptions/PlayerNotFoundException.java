package com.hvz.hvzapi.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class PlayerNotFoundException extends RuntimeException {
	public PlayerNotFoundException(int id) {
		super("No player found with ID: " + id);
	}
	public PlayerNotFoundException(String biteCode) {
		super("No player found with bitecode: " + biteCode);
	}
}
