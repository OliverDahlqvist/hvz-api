package com.hvz.hvzapi.exceptions;

/**
 * Used to throw runtime exceptions when games are not found in the db.
 */
public class GameNotFoundException extends RuntimeException {
    public GameNotFoundException(int id) {
        super("Game not found with ID: " + id);
    }
}
