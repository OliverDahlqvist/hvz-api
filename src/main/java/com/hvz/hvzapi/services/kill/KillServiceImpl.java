package com.hvz.hvzapi.services.kill;

import com.hvz.hvzapi.exceptions.KillNotFoundException;
import com.hvz.hvzapi.models.Kill;
import com.hvz.hvzapi.repositories.KillRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class KillServiceImpl implements KillService {
	
	private final KillRepository killRepository;
	
	public KillServiceImpl(KillRepository killRepository) {
		this.killRepository = killRepository;
	}
	
	@Override
	public Kill findById(Integer id) {
		return killRepository.findById(id).orElseThrow(() -> new KillNotFoundException(id));
	}
	
	@Override
	public Collection<Kill> findAll() {
		return killRepository.findAll();
	}
	
	@Override
	public Kill add(Kill entity) {
		return killRepository.save(entity);
	}
	
	@Override
	public Kill update(Kill entity) {
		return killRepository.save(entity);
	}
	
	@Override
	public void deleteById(Integer id) {
		killRepository.deleteById(id);
	}
}
