package com.hvz.hvzapi.services.kill;

import com.hvz.hvzapi.models.Kill;
import com.hvz.hvzapi.services.CrudService;

public interface KillService extends CrudService<Kill, Integer> {
}
