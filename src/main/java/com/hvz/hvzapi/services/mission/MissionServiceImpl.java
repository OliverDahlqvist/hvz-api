package com.hvz.hvzapi.services.mission;

import com.hvz.hvzapi.exceptions.MissionNotFoundException;
import com.hvz.hvzapi.models.Game;
import com.hvz.hvzapi.models.Mission;
import com.hvz.hvzapi.repositories.GameRepository;
import com.hvz.hvzapi.repositories.MissionRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class MissionServiceImpl implements MissionService {
    private final MissionRepository missionRepository;
    private final GameRepository gameRepository;

    public MissionServiceImpl(MissionRepository missionRepository, GameRepository gameRepository) {
        this.missionRepository = missionRepository;
        this.gameRepository = gameRepository;
    }

    @Override
    public Mission findById(Integer id) {
        return missionRepository.findById(id).orElseThrow(() -> new MissionNotFoundException(id));
    }

    @Override
    public Collection<Mission> findAll() {
        return missionRepository.findAll();
    }

    public Collection<Mission> findAllByGameId(int id) {
        return missionRepository.findAllByGameId(id);
    }

    @Override
    public Mission findByIdAndGameId(int gameId, int missionId) {
        return missionRepository.findByGame_IdAndId(gameId, missionId);
    }

    @Override
    public Mission add(Mission entity) {
        return missionRepository.save(entity);
    }

    @Override
    public Mission update(Mission entity) {
        return missionRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        Optional<Mission> missionResponse = missionRepository.findById(id);
        if(missionResponse.isEmpty()) return;
        Mission mission = missionResponse.get();
        mission.getGame().getMissions().remove(mission);
        missionRepository.deleteById(id);
    }

    @Override
    public ResponseEntity.HeadersBuilder deleteByIdAndGameId(int missionId, int gameId) {
        Optional<Mission> missionResponse = missionRepository.findById(missionId);
        Optional<Game> gameResponse = gameRepository.findById(gameId);

        if(missionResponse.isEmpty() || gameResponse.isEmpty()) return ResponseEntity.notFound();

        Game game = gameResponse.get();
        Mission mission = missionResponse.get();

        if(!game.getMissions().contains(mission)) return ResponseEntity.notFound();

        mission.getGame().getMissions().remove(mission);
        missionRepository.deleteById(missionId);
        return ResponseEntity.ok();
    }
}
