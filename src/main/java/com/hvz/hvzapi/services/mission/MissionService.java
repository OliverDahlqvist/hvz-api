package com.hvz.hvzapi.services.mission;

import com.hvz.hvzapi.models.Mission;
import com.hvz.hvzapi.services.CrudService;
import org.springframework.http.ResponseEntity;

import java.util.Collection;

public interface MissionService extends CrudService<Mission, Integer> {
    /**
     * Finds all mission in the game id.
     * @param id Id of game.
     * @return Collection of found missions.
     */
    Collection<Mission> findAllByGameId(int id);

    /**
     * Finds a specific mission with id in a specific game with id.
     * @param gameId ID of the game.
     * @param missionId ID of the mission.
     * @return Mission from game with specific id.
     */
    Mission findByIdAndGameId(int gameId, int missionId);

    ResponseEntity.HeadersBuilder deleteByIdAndGameId(int missionId, int gameId);
}
