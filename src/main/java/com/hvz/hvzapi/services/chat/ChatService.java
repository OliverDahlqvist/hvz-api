package com.hvz.hvzapi.services.chat;

import com.hvz.hvzapi.models.ChatMessage;
import com.hvz.hvzapi.services.CrudService;

public interface ChatService extends CrudService<ChatMessage, Integer> {
}
