package com.hvz.hvzapi.services.chat;

import com.hvz.hvzapi.models.ChatMessage;
import com.hvz.hvzapi.repositories.ChatRepository;
import com.hvz.hvzapi.exceptions.ChatMessageNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ChatServiceImpl implements ChatService {
	private final ChatRepository chatRepository;
	
	public ChatServiceImpl(ChatRepository chatRepository) {
		this.chatRepository = chatRepository;
	}
	
	@Override
	public ChatMessage findById(Integer id) {
		return chatRepository.findById(id)
				       .orElseThrow(() -> new ChatMessageNotFoundException(id));
	}
	
	@Override
	public Collection<ChatMessage> findAll() {
		return chatRepository.findAll();
	}
	
	@Override
	public ChatMessage add(ChatMessage entity) {
		return chatRepository.save(entity);
	}
	
	@Override
	public ChatMessage update(ChatMessage entity) {
		return chatRepository.save(entity);
	}
	
	@Override
	public void deleteById(Integer id) {
		chatRepository.deleteById(id);
	}
}
