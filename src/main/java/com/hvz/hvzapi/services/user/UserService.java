package com.hvz.hvzapi.services.user;

import com.hvz.hvzapi.models.User;
import com.hvz.hvzapi.services.CrudService;

public interface UserService extends CrudService<User, String> {
    User add(String id, String firstName, String lastName);
}
