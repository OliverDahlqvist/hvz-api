package com.hvz.hvzapi.services.user;

import com.hvz.hvzapi.exceptions.UserNotFoundException;
import com.hvz.hvzapi.models.User;
import com.hvz.hvzapi.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class UserServiceImpl implements UserService {
	private final UserRepository userRepository;
	
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	@Override
	public User findById(String id) {
		return userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
	}
	
	@Override
	public Collection<User> findAll() {
		return userRepository.findAll();
	}
	
	@Override
	public User add(User entity) {
		return userRepository.save(entity);
	}

	@Override
	public User add(String uid, String firstName, String lastName) {
		if(userRepository.existsById(uid)) return null;
		User user = new User();
		System.out.println(firstName + ", " + lastName);
		user.setUid(uid);
		user.setFirstName(firstName);
		user.setLastName(lastName);

		return userRepository.save(user);
	}
	
	@Override
	public User update(User entity) {
		return userRepository.save(entity);
	}
	
	@Override
	public void deleteById(String uid) {
		userRepository.deleteById(uid);
	}
}
