package com.hvz.hvzapi.services.game;

import com.hvz.hvzapi.models.ChatMessage;
import com.hvz.hvzapi.models.Game;
import com.hvz.hvzapi.services.CrudService;

import java.util.Set;

public interface GameService extends CrudService<Game, Integer> {
    Set<ChatMessage> getChat(boolean isHuman, int gameId);
    
    /**
     * Checks if a game exists with the given ID.
     * @param id ID of game to check if it exists.
     * @return True if game with given id exists.
     */
    boolean exists(int id);
}
