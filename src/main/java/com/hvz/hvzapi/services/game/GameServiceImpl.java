package com.hvz.hvzapi.services.game;

import com.hvz.hvzapi.exceptions.GameNotFoundException;
import com.hvz.hvzapi.models.ChatMessage;
import com.hvz.hvzapi.models.Game;
import com.hvz.hvzapi.models.Player;
import com.hvz.hvzapi.repositories.GameRepository;
import com.hvz.hvzapi.repositories.MissionRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;

@Service
public class GameServiceImpl implements GameService {

    private final GameRepository gameRepository;
    private final MissionRepository missionRepository;

    public GameServiceImpl(GameRepository gameRepository, MissionRepository missionRepository) {
        this.gameRepository = gameRepository;
        this.missionRepository = missionRepository;
    }

    @Override
    public Game findById(Integer id) {
        return gameRepository.findById(id).orElseThrow(() -> new GameNotFoundException(id));
    }

    @Override
    public Collection<Game> findAll() {
        return gameRepository.findAll();
    }

    @Override
    public Game add(Game entity) {
        return gameRepository.save(entity);
    }

    @Override
    public Game update(Game entity) {
        return gameRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        var game = gameRepository.findById(id);
        if(game.isEmpty()) return;
        var newGame = game.get();
        newGame.getPlayers().forEach(p -> p.setGame(null));
        missionRepository.deleteAllByGame(newGame);
        gameRepository.deleteById(id);
    }
    
    @Override
    public Set<ChatMessage> getChat(boolean isHuman, int gameId) {
        return gameRepository.getChat(isHuman, gameId);
    }
    
    @Override
    public boolean exists(int id) {
        return gameRepository.existsById(id);
    }
}
