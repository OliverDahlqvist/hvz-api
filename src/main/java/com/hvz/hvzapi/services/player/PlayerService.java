package com.hvz.hvzapi.services.player;

import com.hvz.hvzapi.models.Player;
import com.hvz.hvzapi.services.CrudService;

public interface PlayerService extends CrudService<Player, Integer> {
	Player findByBiteCode(String biteCode);
	Player findByGame_IdAndHvzUser_Uid(int gameId, String userUId);
}
