package com.hvz.hvzapi.services.player;

import com.hvz.hvzapi.models.Player;
import com.hvz.hvzapi.repositories.PlayerRepository;
import com.hvz.hvzapi.exceptions.PlayerNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class PlayerServiceImpl implements PlayerService {
	private final PlayerRepository playerRepository;
	
	public PlayerServiceImpl(PlayerRepository playerRepository) {
		this.playerRepository = playerRepository;
	}
	
	@Override
	public Player findById(Integer id) {
		return playerRepository.findById(id).orElseThrow(
				() -> new PlayerNotFoundException(id));
	}
	
	@Override
	public Collection<Player> findAll() {
		return playerRepository.findAll();
	}
	
	@Override
	public Player add(Player entity) {
		return playerRepository.save(entity);
	}
	
	@Override
	public Player update(Player entity) {
		return playerRepository.save(entity);
	}
	
	@Override
	public void deleteById(Integer id) {
		playerRepository.deleteById(id);
	}
	
	@Override
	public Player findByBiteCode(String biteCode) {
		return playerRepository.findAll().stream().filter(p-> p.getBiteCode().contentEquals(biteCode)).findAny().orElseThrow(
				() -> new PlayerNotFoundException(biteCode));
	}

	@Override
	public Player findByGame_IdAndHvzUser_Uid(int gameId, String userUId) {
		return playerRepository.findByGame_IdAndHvzUser_Uid(gameId, userUId);
	}
}
