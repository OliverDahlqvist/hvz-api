# Human vs Zombies (HvZ)
This repository contains the RESTful API for the web based Java Spring game application called Humans vs Zombies. The project is the final case for Experis Academy's accelerated Fullstack Java graduate program. Detailed description of the project can be found


## Description
Humans vs. Zombies (HvZ) is a game of tag played at schools, camps, neighborhoods,
libraries, and conventions around the world. The game simulates the exponential spread
of a fictional zombie infection through a population. 

## Installation
The application is free to clone straight from gitlab. Type this into your selected git console to get the current main version:
 ```git clone https://gitlab.com/OliverDahlqvist/hvz-fullstack-assignment.git``` 


 ## Install react app locally 
 Clone the repository to your local machine
 Run npm install in a terminal to install the necessary dependencies
 Run npm start to run the application


## Deployment
React application is deployed to heroku. Link to the application:
 https://experis-react-hvz-frontend.herokuapp.com/

## Used Language
Java - version 17

## Used frameworks
Spring Boot
Hibernate

## Database
PostgreSQL 
Postman

## Authentication management
Keyckloak
 
## Contributors
Johanna Olsson @johannaolsson
Oliver Dahlqvist @OliverDahlqvist
Emil Uhlin @Emil-H-Uhlin
Huwaida ALhamdawee @Huwaida-al


